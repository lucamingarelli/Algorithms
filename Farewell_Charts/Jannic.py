import numpy as np
import matplotlib.pyplot as plt, seaborn as sns
from matplotlib.offsetbox import TextArea, DrawingArea, OffsetImage, AnnotationBbox
import matplotlib.image as mpimg
plt.xkcd()

from Farewell_Charts.utils import draw_stick_figure, interp

Y_MIN, Y_MAX = 2020, 2022
DATE1 = 2020 + 2 / 12
DATE2_1 = 2020 + 8/12
DATE2_2 = 2020 + 9.7/12
DATE3 = 2021 + 9 / 12
DATE4 = 2021 + 10 / 12
DATE5 = 2021.3
DATES = [DATE1, DATE2_1, DATE2_2, DATE3, DATE4, DATE5]
LABS = ["Jannic joining", "MBF attempting\nthe steal",
        "", "Jannic\nflies to MAY", "", "CDSW is down\n(again)"]
y = np.array([.5,
              .4, .5,.6,.8, -.6,.4,-.5,-1.8,.5,.99,
              .8, 1,.7,.8, -.6,.2,.8,.95,-1,0,
               .25, .3, .4])
x = np.array([2019.9,
              2020, 2020.1, 2020.2, 2020.3, 2020.4, 2020.5, 2020.7, 2020.75, 2020.8, 2020.9,
              2021, 2021.1, 2021.2, 2021.3, 2021.4, 2021.5, 2021.6, 2021.65, 2021.75, 2021.9,
              2022, 2022.1, 2022.2 ])

# smooth the data
xnew, ynew = interp(x,y)

def plot_optimism(ax, upto=None):
    # setup axes
    sns.despine(ax=ax, offset=15)
    ax.set_ylim(-1.5, 1.5)
    ax.set_yticks([-1, 0, 1])
    ax.set_yticklabels(['despair', '0', 'hope'])
    ax.set_xlim(Y_MIN - 1 / 12, Y_MAX + 3 / 12)
    ax.set_xticks([2020, 2021, 2022.1])
    ax.set_xticklabels([2020, 2021, 2022])
    ax.set_ylabel('BICG Optimism')
    ax.spines['bottom'].set_position('center')
    ax.set_title('2 years & 1 pandemic together', fontsize=20, pad=20)

    # plot market dates
    for date, lab in zip(DATES, LABS):
        ax.axvline(date, -1, 2, linestyle='--', color='k')
        ax.annotate(lab, xy=(date, -1.5), xytext=(date, -1.6),
                xycoords='data', textcoords='data',
                    ha='center', va='top')

    # plot optimism
    x, y = (xnew[xnew < upto], ynew[xnew < upto]) if upto else (xnew, ynew)
    ax.plot(x, y, zorder=20)


fig, ax = plt.subplots(figsize=(14, 6))
plot_optimism(ax, upto=Y_MAX+9.5/12)

ax.add_artist(AnnotationBbox(OffsetImage(mpimg.imread('Farewell_Charts/img/soon.png'),zoom=0.08),(2020.25, 1.3), frameon=False))
for i,j in [(0.02,-.15),(0.04,-.3),(0.06,-.15)]:
    ax.add_artist(AnnotationBbox(OffsetImage(mpimg.imread('Farewell_Charts/img/covid.png'),zoom=0.015),(2020.3+i, 1+j), frameon=False))
ax.add_artist(AnnotationBbox(OffsetImage(mpimg.imread('Farewell_Charts/img/optimise.png'),zoom=0.15),(2020.4, -1.5), frameon=False))
ax.add_artist(AnnotationBbox(OffsetImage(mpimg.imread('Farewell_Charts/img/server.png'),zoom=0.7),(2020.5, .6), frameon=False))
ax.add_artist(AnnotationBbox(OffsetImage(mpimg.imread('Farewell_Charts/img/Strategy.png'),zoom=0.3),(2020.8, 0.6), frameon=False))
ax.add_artist(AnnotationBbox(OffsetImage(mpimg.imread('Farewell_Charts/img/wtf.png'),zoom=0.09),(2021.25, -0.8), frameon=False))
ax.add_artist(AnnotationBbox(OffsetImage(mpimg.imread('Farewell_Charts/img/fly.png'),zoom=0.8),(2021.7, 1.2), frameon=False))
ax.add_artist(AnnotationBbox(OffsetImage(mpimg.imread('Farewell_Charts/img/other_tower.png'),zoom=0.5),(2022, -1.5), frameon=False))

# ax.annotate('Jannic joining SRF', xy=(DATE1, -.9), xytext=(20, 40),
#             xycoords='data', textcoords='offset points', ha='right', va='bottom',
#             arrowprops=dict(arrowstyle='->', lw=2))
ax.annotate("Don't worry man, this thing will be\nover soon and we'll all meet at the office",
            xy=(DATE1+.2, 1), xytext=(50, 60),
            xycoords='data', textcoords='offset points', ha='right', va='bottom',
            arrowprops=dict(arrowstyle='-', lw=0))
ax.annotate("Ok, all optimised on\n that server over there!",
            xy=(2020.5, -1), xytext=(50, 8),
            xycoords='data', textcoords='offset points', ha='right', va='bottom',
            arrowprops=dict(arrowstyle='-', lw=0))
ax.annotate("CDSW...",
            xy=(2020.5, 1), xytext=(15, 4),
            xycoords='data', textcoords='offset points', ha='right', va='bottom',
            arrowprops=dict(arrowstyle='-', lw=0))
ax.annotate('Steal averted!', xy=(DATE2_2, -.8), xytext=(+40, -50),
            xycoords='data', textcoords='offset points', ha='center', va='bottom',
            arrowprops=dict(arrowstyle='->', lw=2))
ax.annotate('No way:\ncounter attack!', xy=(DATE2_2, 1.05), xytext=(0, 0),
            xycoords='data', textcoords='offset points', ha='center', va='bottom',
            arrowprops=dict(arrowstyle='-', lw=0), zorder=20)
ax.annotate("Guys, I'm just\nin the other tower...\nPlus all my work runs\nautomatically on CDSW!",
            xy=(DATE4+.3, -1), xytext=(-10, -20),
            xycoords='data', textcoords='offset points', ha='center', va='bottom',
            arrowprops=dict(arrowstyle='-', lw=0))
ax.annotate("V-shape recovery :(",
            xy=(DATE4+.2, .3), xytext=(20, 60),
            xycoords='data', textcoords='offset points', ha='center', va='bottom',
            arrowprops=dict(arrowstyle='->', lw=2))
# draw_stick_figure(ax, x=.6, y=.35,
#                   quote='Population genetics???',
#                   radius=.02, xytext=(25, -30))

fig.tight_layout()
plt.savefig("Farewell_Charts/img/JannicFarewell.png", dpi=500)
plt.show()


