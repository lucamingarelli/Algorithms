# The standard way of performing multivariate linear regressions in pySpark is as it follows
import numpy as np
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.regression import LinearRegression

def lin_reg_mllib(df, features, target):
    """ Perform multivariate linear regression via pyspark.ml library.
    Args:
    df:               Pyspark dataframe
    features (list):  List of features - these must be columns of df
    target (str):     Dependent variable - this must be a column of df
    """
    assembler = VectorAssembler(inputCols=features,
                                outputCol='features')
    df_out = assembler.transform(df).select(['features', target])
    lr_model = LinearRegression(featuresCol='features', labelCol=target,
                                fitIntercept=True)
    fit_model = lr_model.fit(df_out)
    alpha, *beta = fit_model.intercept, fit_model.coefficients 
    
    return alpha, beta

  
# How can one perform a multivariate linear regression in pySpark without using the ML library?
# Here is an option:

def lin_reg(df, features, target):
    """Perform multivariate linear regression WITHOUT resorting to the pyspark.ml library.
    Args:
    df:               Pyspark dataframe
    features (list):  List of features - these must be columns of df
    target (str):     Dependent variable - this must be a column of df
    Consider that if X = [1, x, w, ..., t], with 1, x, w, t all column vectors of length N, then

              [N,    Σx,   Σw,  ...,   Σt]
              [Σx,  Σx²,   xw,  ...,   xt]
    X.T @ X = [Σw,   wx,  Σw²,  ...,   wt]
              [..., ...,  ...,  ...,  ...]
              [Σt,   tx,   tw,  ...,  Σt²]
    
    Moreover notice that
    
    X.T @ y = [Σy, xy, wy, ..., ty]
    
    Then we have the OLS estimators: (X.T @ X)⁻¹  @	 (X.T @ y).
    """
    _df = df.select(features + [target]).dropna()
    N = _df.count()
    
    sums, dot_pairs, dots_y = [], [], []
    for k, feature1 in enumerate(features):
        # Compute sums
        sums.append(np.float(_df.select(feature1).groupBy().sum().collect()[0][0]))
        # Compute dot(feature, y)
        dots_y.append(np.float(_df.withColumn(f'{feature1}_dot_y', 
                                            _df[feature1]*_df[target])
                                  .select(f'{feature1}_dot_y')
                                  .groupBy().sum().collect()[0][0]))
        
        dot_pairs.append([])
        for feature2 in features[k:]:  # Compute pairs
            feats_dot = f'{feature1}_dot_{feature2}'
            dot_pairs[-1].append(np.float(_df.withColumn(feats_dot, 
                                                     _df[feature1]*_df[feature2])
                                         .select(feats_dot)
                                         .groupBy().sum().collect()[0][0]))
    
    # First construct X.T @ y 
    sum_y = np.float(_df.select(target).groupBy().sum().collect()[0][0])
    XTy = [sum_y] + dots_y
    
    # Now construct X.T @ X
    XTX = np.zeros([len(features)+1]*2)
    XTX[0,0] = N
    XTX[0, 1:] = sums
    for k, row in enumerate(dot_pairs):
        XTX[k+1, k+1:] = row
    # Make symmetric
    XTX = XTX + XTX.T
    
    alpha, *beta = np.linalg.inv(XTX) @ XTy
    
    return alpha, beta
