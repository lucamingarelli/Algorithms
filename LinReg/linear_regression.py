import numpy as np
from sklearn.linear_model import LinearRegression

### UNIVARIATE

x = np.array([1,2,3,4])
y = 2*x - 1 + np.random.randn(4)/10

X = np.array([np.ones(len(y)), x]).T

# Plain
alpha, *beta = np.linalg.inv((X.T @ X)) @ (X.T @ y)

# With Scikit
LM = LinearRegression()
fit = LM.fit(X, y)
fit.coef_, fit.intercept_




### MULTIVARIATE

x = np.array([1, 2, 3, 4])
w = np.array([0, 7, 15, 32])
y = 2 * x + 0.2*w - 1 + np.random.randn(4) / 10

X = np.array([np.ones(len(y)), x, w]).T

# Plain
alpha, *beta = np.linalg.inv((X.T @ X)) @ (X.T @ y)

# With Scikit
LM = LinearRegression()
fit = LM.fit(X, y)
fit.coef_, fit.intercept_