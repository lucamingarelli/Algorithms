import numpy as np
from numpy.random import default_rng
import multiprocessing
import concurrent.futures


def add_threaded2D(a, b, threads=None):
  """Modifies a inplace; beats numpy from around 1e7 operations onwards.
  Args:
    a (numpy.array): Left array to be summed. Modified in place.
    b (numpy.array): Right array to be summed. 
    threads (int or None): Number of threads.
  """
    assert a.shape == b.shape

    threads = threads or multiprocessing.cpu_count()
    executor = concurrent.futures.ThreadPoolExecutor(threads)
    steps = [(t * (a.shape[0] // threads), (t + 1) * (a.shape[0] // threads))
             if t < (threads - 1)
             else (t * (a.shape[0] // threads), a.shape[0])
             for t in range(threads)]

    def _fill(firstrow, lastrow):
        a[firstrow:lastrow] += b[firstrow:lastrow]

    futures = {}
    for i in range(threads):
        args = (_fill,
                steps[i][0],
                steps[i][1])
        futures[executor.submit(*args)] = i
    concurrent.futures.wait(futures)
    executor.shutdown(False)


rng = default_rng(1)
a = rng.standard_normal((int(4e6), 100))
b = rng.standard_normal((int(4e6), 100))

c=a+b
add_threaded2D(a, b, threads=24)
(c==a).all()


%timeit a + b
%timeit add_threaded2D(a, b, threads=24)

#import dask.array as da
#dA, dB = da.array(a), da.array(b)
#%timeit (dA+dB).compute()


import numexpr as ne
%timeit ne.evaluate('a+b')


