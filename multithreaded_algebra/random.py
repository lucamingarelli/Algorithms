"""
Multithreaded generation of random standard normals, optimized for 2D arrays
Adapted from https://numpy.org/doc/stable/reference/random/multithreading.html
"""

from numpy.random import default_rng, SeedSequence
import multiprocessing
import concurrent.futures
import numpy as np

class MultithreadedRNG2D:
    def __init__(self, shape, seed=None, threads=None):

        self.threads = threads or multiprocessing.cpu_count()
        self.shape = shape
        self.executor = concurrent.futures.ThreadPoolExecutor(threads)
        
        seq = SeedSequence(seed)
        self._random_generators = [default_rng(s) for s in seq.spawn(threads)]
        
        self.values = np.empty(shape)
        self.steps = [(t * (shape[0] // threads), (t + 1) * (shape[0] // threads))
                      if t < (threads - 1) else 
                      (t * (shape[0] // threads), shape[0])
                      for t in range(threads)]
        
    def fill(self):
        def _fill(random_state, out, firstrow, lastrow):
            random_state.standard_normal(out=out[firstrow:lastrow])

        futures = {}
        for i in range(self.threads):
            args = (_fill,
                    self._random_generators[i],
                    self.values,
                    self.steps[i][0],
                    self.steps[i][1])
            futures[self.executor.submit(*args)] = i
        concurrent.futures.wait(futures)

    def __del__(self):
        self.executor.shutdown(False)
        

# No multithreading - 7.97 s per loop
rng = default_rng(seed=1)
%timeit rng.standard_normal((int(4e6), 100))

# Multithreading - 411 ms per loop
print(multiprocessing.cpu_count())
mrng = MultithreadedRNG2D((int(4e6), 100), seed=1, threads=24)
%timeit mrng.fill()
