import numpy as np, matplotlib.pyplot as plt
import matplotlib.patches as patches

ECB_CMAP = [(0, 50, 153), (255, 180, 0), (255, 75, 0), (101, 184, 0), (0, 177, 234), (0, 120, 22), (129, 57, 198),
            (92, 92, 92), (152, 161, 208), (253, 221, 167), (246, 177, 131), (206, 225, 175), (215, 238, 248),
            (141, 184, 141), (174, 151, 199), (169, 169, 169), (217, 217, 217)]
ECB_CMAP = [tuple(c / 255 for c in C) for C in ECB_CMAP]
RED = (192/255, 0, 0)
def set_ECB_plot_style():
    import matplotlib as mpl
    plt.rcParams['axes.facecolor'] = 'white'
    mpl.rc('axes', edgecolor='lightgrey')
    mpl.rcParams['xtick.major.size'] = 0
    mpl.rcParams['ytick.major.size'] = 0
    mpl.rcParams['lines.linewidth'] = 2
    mpl.rcParams['axes.grid'] = True
    mpl.rcParams['grid.alpha'] = 0.75
    mpl.rcParams["grid.color"] = "lightgrey"
set_ECB_plot_style()
plt.rcParams['text.latex.preamble'] = r'\usepackage{amsmath}'
plt.rcParams['text.usetex'] = True


x = np.linspace(0.0001, 5, 500)

plt.figure(figsize=(10,4))
plt.plot(x, np.log(x+.1)-.4, color=ECB_CMAP[0], linewidth=2)
plt.plot(x, np.log(x-.06)+1, '--',color=ECB_CMAP[0], linewidth=2)
plt.plot(x, x*.225 + 1.5, color='gray', linestyle='dotted', linewidth=2)
plt.plot(x, x*.225 + .15, color='gray', linestyle='dotted', linewidth=2)
plt.text(5.1, 1, r'$y$', fontdict=dict(size=50, color=ECB_CMAP[0]))
plt.text(5.1, 2.5, r'$y^*$', fontdict=dict(size=50, color=RED))
plt.arrow(4.9, 1.3, 0, 1, color=RED, width=0.04, zorder=20)
plt.xlim(0, 6)
plt.ylim(-2, 3)
plt.gca().set_xticklabels([])
plt.gca().set_yticklabels([])
plt.tight_layout()
plt.savefig('BICG/img/up.svg')
plt.show()


plt.figure(figsize=(10,4))
plt.plot(x,np.sqrt(x),color=ECB_CMAP[0])
plt.plot(x,2*np.sqrt(x+.1)-.5, '--',color=ECB_CMAP[0])
plt.plot(x, x*.48 + 1.65, color='gray', linestyle='dotted')
plt.plot(x, x*.2 + 1.27, color='gray', linestyle='dotted')
plt.text(5.1, 2.1, r'$y$', fontdict=dict(size=50, color=ECB_CMAP[0]))
plt.text(5.1, 4, r'$y^*$', fontdict=dict(size=50, color=RED))
plt.gca().add_patch(patches.FancyArrowPatch((4.8, 2.35), (4.7, 3.75),
                                            connectionstyle="arc3,rad=.7",
                                            color=RED, zorder=20,
                                            arrowstyle="Simple, tail_width=4, head_width=20, head_length=10"))
plt.xlim(0, 6)
plt.ylim(0, 5)
plt.gca().set_xticklabels([])
plt.gca().set_yticklabels([])
plt.tight_layout()
plt.savefig('BICG/img/rot.svg')
plt.show()

