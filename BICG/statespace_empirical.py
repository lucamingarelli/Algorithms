import matplotlib.pyplot as plt
plt.rcParams['text.latex.preamble'] = r'\usepackage{amsmath}' #for \text command
#Options
params = {'text.usetex' : True}
plt.rcParams.update(params)
text_params = {'ha': 'center', 'va': 'center', 'family': 'sans-serif',
               'fontweight': 'bold'}
plt.rcParams['xtick.major.pad']='12'
plt.rcParams['ytick.major.pad']='12'

ALPHA = 0.2
fig = plt.figure(figsize=(10,10))

rec1 = plt.Rectangle([0]*2, width=5, height=5,  color='red', hatch='xx', fc='white', alpha=ALPHA)
rec2_0 = plt.Rectangle([.5]*2, width=5, height=5, color='white')
rec2_1 = plt.Rectangle([.5]*2, width=5, height=5, color='orange', hatch='o', fc='white', alpha=ALPHA)
rec3_0 = plt.Rectangle([1]*2, width=5, height=5, color='white')
rec3_1 = plt.Rectangle([1]*2, width=5, height=5, color='royalblue', hatch='..', fc='white', alpha=ALPHA)
for rc in [rec1, rec2_0, rec2_1, rec3_0, rec3_1]:
    plt.gca().add_patch(rc)

plt.plot([0,5],[.5]*2, 'k--', linewidth=3)
plt.plot([0,5],[1]*2, 'k--', linewidth=3)
plt.plot([.5]*2,[0, 5], 'k--', linewidth=3)
plt.plot([1]*2,[0, 5], 'k--', linewidth=3)

fontsize=25
plt.xticks([0.5, 1, 4],[r'$\langle\mathcal{D}^K\rangle$', r'$\langle\mathcal{T}^K\rangle$', r'$K$[fraction of RWA]'], fontsize=fontsize)
plt.yticks([0.5, 1, 4],[r'$\langle\mathcal{D}^C\rangle$', r'$\langle\mathcal{T}^C\rangle$', r'$C$'], fontsize=fontsize)

plt.axis('scaled')
plt.xlim(0,4)
plt.ylim(0,4)
ymax = xmax = 4
hw = 1./40.*ymax
hl = 1./40.*xmax
lw = 1. # axis line width

plt.arrow(0, 0, 4.2, 0, fc='k', ec='k', lw=4,
             head_width=hw, head_length=hl, overhang=0,
             length_includes_head=True, clip_on=False)
plt.arrow(0, 0, 0., 4.2, fc='k', ec='k', lw=4,
         head_width=hw, head_length=hl, overhang=0,
         length_includes_head=True, clip_on=False)

# LEGEND
from matplotlib.patches import Patch
cmap = plt.cm.coolwarm
custom_lines = [Patch(facecolor='white', hatch='..', edgecolor='royalblue', label='Color Patch', alpha=ALPHA),
                Patch(facecolor='white', hatch='o', edgecolor='orange', label='Color Patch', alpha=ALPHA),
                Patch(facecolor='white', hatch='xx', edgecolor='red', label='Color Patch', alpha=ALPHA)]
plt.legend(custom_lines, [r'Healthy ($s_j=0$)',
                          r'Distress ($s_j=1$)',
                          r'Default  ($s_j=2$)'],
           prop={'size': 24},bbox_to_anchor=(-.05, 1.02), loc='lower left',frameon=False)


from matplotlib.patches import Ellipse
banks_mean = Ellipse(xy=(2, 2), width=0.5, height=0.2,
                     edgecolor='tab:blue', fc='tab:blue',
                     alpha=0.5, lw=1)
for rc in [banks_mean]:
    plt.gca().add_patch(rc)

plt.tight_layout()
plt.savefig('BICG/img/statespace_empirical.png', dpi=500)
plt.savefig('BICG/img/statespace_empirical.svg')
plt.show()
