""" When transmitting the Morse code, the international standard specifies that:

"Dot" – is 1 time unit long.
"Dash" – is 3 time units long.
Pause between dots and dashes in a character – is 1 time unit long.
Pause between characters inside a word – is 3 time units long.
Pause between words – is 7 time units long.

When the message is written in Morse code,
a single space is used to separate the character codes
and 3 spaces are used to separate words.

See here: https://github.com/8fdafs2/Codewars-Solu-Python/blob/master/src/kyu2_Decode_the_Morse_code_for_real.py
and here: https://www.codewars.com/kata/54acd76f7207c6a2880012bb/python
"""
from decode_morse.morse_dictionary import MORSE_CODE
from itertools import groupby
import re


def decodeBitsAdvanced(bits):
    bits = bits.strip("0")
    if not bits: return ""

    consecutive_group_sizes = [(label, sum(1 for _ in group)) for label, group in groupby(bits)]

    min_length = min(g_size for label, g_size in consecutive_group_sizes)
    max_lenght_1 = max(g_size for label, g_size in consecutive_group_sizes if label == "1")
    dashlen = max_lenght_1 if max_lenght_1 != min_length else min_length * 3

    cleaned_bits = []
    for b, g in groupby(bits):
        len_g = len(list(g))
        if len_g < (min_length + dashlen) / 2:
            cleaned_bits.append(b)
        elif len_g <= dashlen + 2:
            cleaned_bits.append(b*3)
        else:
            cleaned_bits.append(7*b)

    return ("".join(cleaned_bits).replace('0'*7, '   ')
                                 .replace('1'*3, '-')
                                 .replace('0'*3, ' ')
                                 .replace('1', '.')
                                 .replace('0', ''))

def decodeBitsAdvanced_KM(bits):
    class Cluster(object):
        def __init__(self, center, weight, min=None, max=None, name=None):
            self.center = center
            self.weight = weight
            self.min, self.max = min or center, max or center
            self.name = name
        def __repr__(self):
            return f'Cluster {self.name} (center={self.center}, [mn,mx]=[{self.min},{self.max}], weight={self.weight})'

    bits = bits.strip('0')
    if not bits: return ''

    tokens = re.compile('(0+)').split(bits.strip('0'))
    token_lengths = sorted(len(token) for token in tokens)
    minLen, maxLen = token_lengths[0], token_lengths[-1]
    lenRange = maxLen - minLen
    # 3-means clustering, one for each pause size (symbol: 1, char: 3, word:7)
    clusters = tuple(Cluster(center=minLen + lenRange * x / 8, weight=0,
                             min=minLen + lenRange * x / 8, max=minLen + lenRange * x / 8, name=x)
                     for x in (1, 3, 7))
    for sample in token_lengths:
        C = min(clusters, key=lambda C: abs(sample - C.center))  # Most likely cluster for sample
        # Adjust cluster center, increasing its weight by 1 for each sample
        C.center = (C.center * C.weight + sample) / (C.weight + 1)
        C.weight += 1
        # Extend cluster range if needed
        C.min = min(C.min, sample)
        C.max = max(C.max, sample)
    # Fill the gaps if we really have less than 3 clusters
    clusters = [C for C in clusters if C.weight]  # Filter out empty clusters
    if len(clusters) == 2:
        # If 1 and 7 are present while 3 is not, add a synthetic cluster for 3
        if float(clusters[1].min) / clusters[0].max >= 5:
            clusters.insert(1,
                            Cluster(center=(clusters[0].max + clusters[1].min) / 2,
                                    weight=0,
                                    min=clusters[0].max + 1,
                                    max=clusters[1].min - 1))
    if len(clusters) < 3:  # If only 1 is present (or only 1 and 3 are), add synthetic clusters for 3 and 7 (or just 7)
        limit = clusters[-1].max + 1
        clusters.extend(Cluster(center=limit, weight=0, name='Synthetic-3')
                        for _ in range(3 - len(clusters)))
    # Calculating edges between dots and dashes, and dashes and word pauses
    maxDot = (clusters[0].max + clusters[1].min) / 2
    maxDash = (clusters[1].max + clusters[2].min) / 2
    # Transcoding
    ret = []
    for token in tokens:
        if token[0] == '1':       ret.append('.' if len(token) <= maxDot else '-')
        elif len(token) > maxDot: ret.append(' ' if len(token) <= maxDash else '   ')
    return ''.join(ret)

def decode_bits_OLD(bits):
    INF = 1e19
    bits = bits.strip('0')
    time_unit = 1
    if len(bits)>1:
        consecutive_group_sizes = [(label, sum(1 for _ in group))
                                   for label, group in groupby(bits)]
        time_unit = min(min([counts for lab, counts in consecutive_group_sizes if lab=='0'] or [INF]),
                        min([counts for lab, counts in consecutive_group_sizes if lab == '1'] or [INF]))

    symbl_pause = '0' * time_unit
    chars_pause = '0' * 3 * time_unit
    words_pause = '0' * 7 * time_unit
    dot = '1' * time_unit
    dash = '1' * 3 * time_unit
    fix_bits = lambda b: '.' if b==dot else '-' if b==dash else '' if b=='' else None

    bits_split = [[c.split(symbl_pause) for c in w.split(chars_pause)] for w in bits.split(words_pause)]
    bits_time_corrected = [[[fix_bits(b) for b in c] for c in w] for w in bits_split]
    morse_code = '   '.join(' '.join(''.join(c) for c in w) for w in bits_time_corrected)
    return morse_code

def decode_bits(bits):
    # 1) Remove trailing and leading 0's
    bits = bits.strip('0')
    if not bits: return ''
    # 2) Find the least amount of occurrences of either a 0 or 1, and that is the time hop
    split_bits = re.findall(r'1+|0+', bits)
    time_unit = min(len(m) for m in split_bits)
    # 3) Hop through the bits and translate to morse
    return (bits[::time_unit].replace('1'*3, '-')
                             .replace('1','.')
                             .replace('0'*7,'   ')
                             .replace('0'*3,' ')
                             .replace('0',''))

def decode_morse(morse_code):
    morse_split = [w.split() for w in morse_code.strip().split(' '*3)]
    translation = ' '.join(''.join(MORSE_CODE[c] for c in w) for w in morse_split)
    return translation

def _test():
    from decode_morse.manual_test_cases import tests

    passed = 0
    for n, test in enumerate(tests):
        print(f"Test {n}: ", end='')
        try:
            print(f"{decode_morse(decodeBitsAdvanced(test))}")
            passed += 1
        except:
            print('Failed.')
    print(f'{passed}/{len(tests)} tests passed.\n{len(tests)-passed} tests failed.')

if __name__=='__main__':
    # bits = '1100110011001100000011000000111111001100111111001111110000000000000011001111110011111100111111000000110011001111110000001111110011001100000011'
    # bits = '0000000011011010011100000110000001111110100111110011111100000000000111011111111011111011111000000101100011111100000111110011101100000100000'
    # morse_code = decode_bits(bits)  # = ".... . -.--   .--- ..- -.. ."
    # print(decode_morse(morse_code))
    _test()



"""
In this kata you have to deal with "real-life" scenarios, when Morse code transmission speed slightly varies throughout the message as it is sent by a non-perfect human operator. Also the sampling frequency may not be a multiple of the length of a typical "dot".
For example, the message HEY JUDE, that is ···· · −·−−   ·−−− ··− −·· · may actually be received as follows:

0000000011011010011100000110000001111110100111110011111100000000000111011111111011111011111000000101100011111100000111110011101100000100000

As you may see, this transmission is generally accurate according to the standard, but some dots and dashes and pauses are a bit shorter or a bit longer than the others.

Note also, that, in contrast to the previous kata, the estimated average rate (bits per dot) may not be a whole number – as the hypotetical transmitter is a human and doesn't know anything about the receiving side sampling rate.

For example, you may sample line 10 times per second (100ms per sample), while the operator transmits so that his dots and short pauses are 110-170ms long. Clearly 10 samples per second is enough resolution for this speed (meaning, each dot and pause is reflected in the output, nothing is missed), and dots would be reflected as 1 or 11, but if you try to estimate rate (bits per dot), it would not be 1 or 2, it would be about (110 + 170) / 2 / 100 = 1.4. Your algorithm should deal with situations like this well.

Also, remember that each separate message is supposed to be possibly sent by a different operator, so its rate and other characteristics would be different. So you have to analyze each message (i. e. test) independently, without relying on previous messages. On the other hand, we assume the transmission charactestics remain consistent throghout the message, so you have to analyze the message as a whole to make decoding right. Consistency means that if in the beginning of a message '11111' is a dot and '111111' is a dash, then the same is true everywhere in that message. Moreover, it also means '00000' is definitely a short (in-character) pause, and '000000' is a long (between-characters) pause.

That said, your task is to implement two functions:

1. Function decodeBitsAdvanced(bits), that should find an estimate for the transmission rate of the message, take care about slight speed variations that may occur in the message, correctly decode the message to dots ., dashes - and spaces (one between characters, three between words) and return those as a string. Note that some extra 0's may naturally occur at the beginning and the end of a message, make sure to ignore them. If message is empty or only contains 0's, return empty string. Also if you have trouble discerning if the particular sequence of 1's is a dot or a dash, assume it's a dot. If stuck, check this for ideas.

2. Function decodeMorse(morseCode), that would take the output of the previous function and return a human-readable string. If the input is empty string or only contains spaces, return empty string.

NOTE: For coding purposes you have to use ASCII characters . and -, not Unicode characters.

The Morse code table is preloaded for you as MORSE_CODE dictionary, feel free to use it. For C, the function morse_code acts like the dictionary. For C++ and Scala, a map is used. For C#, it's called Preloaded.MORSE_CODE. For Racket, a hash called MORSE-CODE is used.

(hash-ref MORSE-CODE "".-.") ; returns "C"
Of course, not all messages may be fully automatically decoded. But you may be sure that all the test strings would be valid to the point that they could be reliably decoded as described above, so you may skip checking for errors and exceptions, just do your best in figuring out what the message is!

Good luck!
"""





