def last_digit(a, b):
    """a^b mod 10 == (a mod 10)^(b mod 4 + 4) mod 10"""
    if b == 0: return 1
    return (a % 10) ** (b % 4 + 4) % 10



if __name__=='__main__':
    from last_digit_of_large_numbers.test_cases import tests
    for a, b, res in tests:
        assert last_digit(a,b)==res
