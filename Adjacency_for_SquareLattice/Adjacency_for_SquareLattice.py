import numpy as np
from numba import jit


@jit(nopython=True)
def adjacency_from_square_lattice(M, periodic_bc=False):
    Nx, Ny = M.shape
    A = np.zeros((Nx**2, Ny**2))
    for i in range(Nx):
        for j in range(Ny):
            if j>0 and M[i, j-1] == M[i, j]:
                A[i*Ny + j,     i*Ny + (j-1)] = 1
                # A[i*Ny + (j-1), i*Ny + j] = 1
            if i>0 and M[i-1, j] == M[i, j]:
                A[i*Ny + j,     (i-1)*Ny + j] = 1
                # A[(i-1)*Ny + j, i*Ny + j] = 1
            if periodic_bc:
                if i==Nx-1 and M[0, j] == M[i,j]:
                    A[i*Ny + j, j] = 1
                    # A[j, i * Ny + j] = 1
                if j==Ny-1 and M[i, 0] == M[i, j]:
                    A[i*Ny + j, i*Ny] = 1
                    # A[i * Ny, i * Ny + j] = 1
    return A


if __name__=='__main__':
    M = np.array([[1, 1, 1],
                  [1, 0, 1],
                  [1, 1, 1]], dtype=np.int8)

    M = np.array([[1, 1],
                  [1, 0]], dtype=np.int8)

    M = np.array([[1, 1],
                  [1, 1]], dtype=np.int8)

    A = np.array([[0, 1, 1, 0],
                  [1, 0, 0, 1],
                  [1, 0, 0, 1],
                  [0, 1, 1, 0]]) #Expected

    A = adjacency_from_square_lattice(M)
