import numpy as np, matplotlib.pyplot as plt, seaborn as sns
sns.set()

# PLOT 1
x = np.linspace(0, 2.5, 100)
f,g = 2*x**2 -x -1, x**2
plt.plot(x, f)
plt.plot(x, g)
plt.plot(x, f/g, 'k--')
plt.ylim(-5, f.max())
plt.xlim(x.min(),x.max())
plt.show()

# PLOT 2

f, g
plt.plot(x, np.tan(x)/x)
plt.show()