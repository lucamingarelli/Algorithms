import numpy as np
from fast_matrix_exp import fast_2b2mat_exp

def fib(n):
    """This is the fastest way to compute arbitrary precision Fibonacci numbers.
    Complexity: O(log(n)).
    See the Wiki page for details: https://en.wikipedia.org/wiki/Fibonacci_number#Matrix_form
    """
    # Keep `dtype=np.object` to preserve arbitrary precision!
    M = np.array([[0, 1], [1, 1]], dtype=np.object)
    # M_n = [[f(n-1), f(n)],
    #        [  f(n), f(n+1)]]
    M_n = np.linalg.matrix_power(M, n)

    return M_n[0, 1]

def fib2(n):
    """Same as above, with my mat_exp.
    This is much faster!!"""
    return fast_2b2mat_exp([[0, 1], [1, 1]], n)[0][1]



if __name__=='__main__':
    import time, matplotlib.pyplot as plt, seaborn as sns
    from tqdm import tqdm
    sns.set()
    times_fib, times_fib2 = [], []
    sizes = np.logspace(0, 7, 50)
    for n in tqdm(sizes):
        t0 = time.time()
        fib(int(n))
        times_fib.append(time.time()-t0)
        t0 = time.time()
        fib2(int(n))
        times_fib2.append(time.time() - t0)
    plt.plot(sizes, times_fib, label="np.linalg.matrix_power")
    plt.plot(sizes, times_fib2, label="fast_2b2mat_exp")
    plt.show()
