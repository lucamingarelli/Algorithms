def height(n, m):
    """
    Using the summation of binomial coefficient(sum_{i=0}^k{N\choose i})
    for t trial, such that ∑tCi from i = 1 to number of eggs.
    Use the fact:
    {N/choose i} = {N/choose i-1}*frac{N-i+1}{i}
    to calculate each term from the preceding."""
    height, tot = 0, 1
    for i in range(1, n + 1):
        tot = tot * (m - i + 1) // i
        height += tot
    return height