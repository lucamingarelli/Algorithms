import numpy as np

def fast_2b2mat_exp(x, n):
    """Can take either np.array of list of lists."""
    (e0, e1), (e2, e3) = (x0, x1), (x2, x3) = x
    # x = [[e0, e1],
    #      [e2, e3]]
    # Fast O(log(n)) exponentiation of the matrix
    for b in bin(n)[3:]:  # x = x * x
        e12 = e1 * e2
        e0, e1, e2, e3 = (e0**2 + e12,
                          e0*e1 + e1*e3,
                          e2*e0 + e3*e2,
                          e12 + e3**2)
        if b=="1":  # x = x * x0
            e0, e1, e2, e3 = (e0*x0 + e1*x2,
                              e0*x1 + e1*x3,
                              e2*x0 + e3*x2,
                              e2*x1 + e3*x3)
    return [[e0, e1], [e2, e3]]


def exp_by_squaring_iterative(x, n):
    """For any square np.matrix or np.array size."""
    x0 = np.copy(x)
    # Fast O(log(n)) exponentiation of the matrix
    for b in bin(n)[3:]:  # x = x * x
        x = x @ x
        if b=="1":  # x = x * x0
            x = x @ x0
    return x