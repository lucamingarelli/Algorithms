import numpy as np, pandas as pd
import holoviews as hv, hvplot.pandas, holoviews.plotting.mpl
hv.extension('bokeh')
hv.output(fig='svg')
renderer = hv.plotting.mpl.MPLRenderer.instance(dpi=500)

# create a large data sample
data = np.random.normal(size=[500000, 2])
df = pd.DataFrame(data=data,
                  columns=['col1', 'col2'])

# use setting datashade=True for large data visualization
plot = df.hvplot.scatter(x='col1', y='col2', datashade=True)


renderer.show(plot)







