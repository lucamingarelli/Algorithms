import numpy as np, pandas as pd
import hvplot.pandas, holoviews.plotting.mpl
import holoviews as hv
import holoviews.operation.datashader as hd
hd.shade.cmap=["lightblue", "darkblue"]
hv.extension("bokeh")
hv.output(fig='svg')
renderer = hv.plotting.mpl.MPLRenderer.instance(dpi=300)


import datashader as ds, matplotlib.pyplot as plt
import datashader.transfer_functions as tf
from bokeh.plotting import show
from collections import OrderedDict as odict

num=100_000
np.random.seed(1)

dists = {cat: pd.DataFrame(odict([('x',np.random.normal(x,s,num)),
                                  ('y',np.random.normal(y,s,num)),
                                  ('val',val),
                                  ('cat',cat)]))
         for x,  y,  s,  val, cat in
         [(  2,  2, 0.03, 10, "d1"),
          (  2, -2, 0.10, 20, "d2"),
          ( -2, -2, 0.50, 30, "d3"),
          ( -2,  2, 1.00, 40, "d4"),
          (  0,  0, 3.00, 50, "d5")] }

df = pd.concat(dists,ignore_index=True)
df["cat"]=df["cat"].astype("category")
points = hv.Points(df.sample(10000))

renderer.show(points)

hv.output(backend="matplotlib")
agg = ds.Canvas().points(df,'x','y')
plot = hd.datashade(points)  +  hd.shade(hv.Image(agg)) + hv.RGB(np.array(tf.shade(agg).to_pil()), bounds=(-10,-10,10,10))

renderer.show(plot)


