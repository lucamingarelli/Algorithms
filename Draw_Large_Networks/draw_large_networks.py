import pandas as pd, numpy as np
import graph_tool.all as gt
import matplotlib as mpl
import matplotlib.pyplot as plt

df = pd.read_csv('~/Desktop/edg_all.csv')
df = df.head(100_000)

G = gt.Graph(directed=False)
name = G.add_edge_list(df.values, hashed=True)
pos = gt.sfdp_layout(G)


gt.graph_draw(gt.GraphView(G), pos, output="price.pdf")
plt.show()

import numpy as np
import sklearn.metrics
A = np.array([[5,0,4,4,2,5],
              [2,0,0,2,1,2]])
sklearn.metrics.pairwise.cosine_similarity(A)

def CS(x, y):
    return (x@y) / np.sqrt((x@x)*(y@y))

def MCS0(x, y, k1, k2):
    return (x@y) / (k1*k2)

def MCS(x, y, k1, k2):
    N = k1*k2 / (x.sum()*y.sum())
    return CS(x,y) / N

k1, k2 = A[0].sum()*2, A[1].sum()*2
CS(A[0], A[1])
MCS0(A[0], A[1], k1, k2)
MCS(A[0], A[1], k1, k2)


##########################################
##########################################

import datashader as ds
import datashader.transfer_functions as tf
from datashader.layout import random_layout, circular_layout, forceatlas2_layout
from datashader.bundling import connect_edges, hammer_bundle

import holoviews as hv
import holoviews.operation.datashader as hd
hd.shade.cmap=["lightblue", "darkblue"]
hv.extension("bokeh", "matplotlib")
hv.output(backend="matplotlib")


df.columns = ['source', 'target']
edges = df
nodes = pd.DataFrame(np.unique(edges.values.flatten()), columns=['name'])
D = nodes.reset_index().set_index('name').to_dict()['index']
edges.source = edges.source.map(D)
edges.target = edges.target.map(D)


fa  = forceatlas2_layout(nodes=nodes, edges=edges)
cvsopts = dict(plot_height=400, plot_width=400)

def nodesplot(nodes, name=None, canvas=None, cat=None):
    canvas = ds.Canvas(**cvsopts) if canvas is None else canvas
    aggregator=None if cat is None else ds.count_cat(cat)
    agg=canvas.points(nodes,'x','y',aggregator)
    return tf.spread(tf.shade(agg, cmap=["#FF3333"]), px=3, name=name)

tf.Images(nodesplot(fa, "Circular layout"))


def edgesplot(edges, name=None, canvas=None):
    canvas = ds.Canvas(**cvsopts) if canvas is None else canvas
    return tf.shade(canvas.line(edges, 'x', 'y', agg=ds.count()), name=name)


def graphplot(nodes, edges, name="", canvas=None, cat=None):
    if canvas is None:
        xr = nodes.x.min(), nodes.x.max()
        yr = nodes.y.min(), nodes.y.max()
        canvas = ds.Canvas(x_range=xr, y_range=yr, **cvsopts)

    np = nodesplot(nodes, name + " nodes", canvas, cat)
    ep = edgesplot(edges, name + " edges", canvas)
    return tf.stack(ep, np, how="over", name=name)


fa_d = graphplot(fa, connect_edges(fa,edges), "Force-directed")

tf.Images(fa_d)
plt.show()

