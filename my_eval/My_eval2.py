import re
from operator import mul, truediv as div, add, sub

OPS = {'*': mul, '/': div, '-': sub, '+': add}


def my_eval2(expression):
    expression = expression.replace('++', '+')
    tokens = re.findall(r'[.\d]+|[()+*/-]', expression)
    return parse_AddSub(tokens, 0)[0]


def parse_AddSub(tokens, iTok):
    v, iTok = parse_MulDiv(tokens, iTok)

    while iTok < len(tokens) and tokens[iTok] != ')':
        tok = tokens[iTok]
        if tok in '-+':
            v2, iTok = parse_MulDiv(tokens, iTok + 1)
            v = OPS[tok](v, v2)

    return v, iTok


def parse_MulDiv(tokens, iTok):
    v, iTok = parse_Term(tokens, iTok)

    while iTok < len(tokens) and tokens[iTok] in '*/':
        op = tokens[iTok]
        v2, iTok = parse_Term(tokens, iTok + 1)
        v = OPS[op](v, v2)

    return v, iTok


def parse_Term(tokens, iTok):
    tok = tokens[iTok]

    if tok == '(':
        v, iTok = parse_AddSub(tokens, iTok + 1)
        if iTok < len(tokens) and tokens[iTok] != ')': raise Exception()

    elif tok == '-':
        v, iTok = parse_Term(tokens, iTok + 1)
        v, iTok = -v, iTok - 1

    else:
        v = float(tok)

    return v, iTok + 1


if __name__=='__main__':
    tests = [
        "1 + 1",
        "8/16",
        "3 -(-1)",
        "2 + -2",
        "10- 2- -5",
        "(((10)))",
        "3 * 5",
        "-7 * -(6 / 3)",
        "-22 / -61 * -35 * 2",
        "-63 - 85 / 75 + 47 / 36 / 67 * -46 / 99",
        "-(-31) / (64 - 36 / -(20)) + (-40 + (((-(-25 - -3)))) * 55)",
        "31/65.8+(-40++22*55)"
    ]
    for test in tests:
        print(my_eval2(test), eval(test))