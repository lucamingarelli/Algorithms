conv = lambda x: float(x) if '.' in x else int(x)
OPERATORS = {'*': lambda x, y: conv(x) * conv(y),
             '/': lambda x, y: conv(x) / conv(y),
             '+': lambda x, y: conv(x) + conv(y),
             '-': lambda x, y: conv(x) - conv(y)}

def _find_first_op_pos(expr, operators=None):
    operators = operators or OPERATORS.keys()
    for n, c in enumerate(expr):
        if c in operators:
            return n
    return None

def _find_last_op_pos(expr, operators=None):
    idx = _find_first_op_pos(expr[::-1], operators=operators)
    if idx:
        return len(expr) - idx -1
    return None

def _split_expr(expr):
    # 1) Fix consecutive signs
    for v, vr in [(' ', ''), ('--','+'),('+-','-'),('-+','-'),('++','+')]:
        expr = expr.replace(v, vr)
    expr = list(expr)
    if expr[0] == '-':  # Correct sign if needed
        expr.pop(0)
        expr[0] = '-' + expr[0]

    pos = 0
    op_pos = _find_first_op_pos(expr, operators=['*', '/'])
    if op_pos and (expr[op_pos + 1] in ['+', '-']):
        op = expr.pop(op_pos + 1)
        expr[op_pos + 1] = op + expr[op_pos + 1]
        pos = op_pos + 1

    while pos<len(expr):
        op_pos = _find_first_op_pos(expr[pos:], operators=['*', '/'])
        if op_pos is not None:
            op_pos += pos
            if expr[op_pos+1] in ['+', '-']:
                op = expr.pop(op_pos+1)
                expr[op_pos+1] = op + expr[op_pos+1]
        pos += 1

    # 2) Join multiple digits numbers
    op_pos = _find_first_op_pos(expr)
    pos = -1
    if op_pos: expr = ["".join(expr[:op_pos])] + expr[op_pos:]
    else: return ["".join(expr)]
    while True:
        pos += 2
        op_pos = _find_first_op_pos(expr[pos + 1:])
        if op_pos:
            op_pos += pos + 1
            expr = expr[:pos + 1] + ["".join(expr[pos + 1:op_pos])] + expr[op_pos:]
        else:
            expr = expr[:pos + 1] + ["".join(expr[pos + 1:])]
            break
    return expr

def _eval_expr(expr):
    expr = _split_expr(expr)

    while '*' in expr or '/' in expr:
        idx = _find_first_op_pos(expr, operators=['*', '/'])
        res = OPERATORS[expr[idx]](expr[idx - 1], expr[idx + 1])
        expr = expr[:idx - 1] + [str(res)] + expr[idx + 2:]
    while '+' in expr or '-' in expr:
        idx = _find_first_op_pos(expr)
        res = OPERATORS[expr[idx]](expr[idx - 1], expr[idx + 1])
        expr = expr[:idx - 1] + [str(res)] + expr[idx + 2:]

    return expr[0]

def _reduce_expr(expr):
    if '(' not in expr:
        return _eval_expr(expr)
    else:
        expr = list(expr.replace(' ', ''))
        first, last = None, None
        for n, c in enumerate(expr):  # Find parenthesis
            if c == '(': first = n + 1
            if c == ')':
                last = n
                break
        res = _eval_expr(''.join(expr[first:last]))
        if expr[first - 2] == '-':  # Correct sign if needed
            res = str(-conv(res))
            if expr[first - 3] not in OPERATORS.keys() and first!=2:
                expr[first - 2] = '+'
            else:
                expr.pop(first - 2)
                first -= 1
                last -= 1
        reduced_expr = expr[:first-1] + [res] + expr[last + 1:]
        return ''.join(reduced_expr)


def my_eval(expression):
    while '(' in expression:
        expression = _reduce_expr(expression)
    return conv(_eval_expr(expression))








if __name__=='__main__':
    tests = [
        "1 + 1",
        "8/16",
        "3 -(-1)",
        "2 + -2",
        "10- 2- -5",
        "(((10)))",
        "3 * 5",
        "-7 * -(6 / 3)",
        "-22 / -61 * -35 * 2",
        "-63 - 85 / 75 + 47 / 36 / 67 * -46 / 99",
        "-(-31) / (64 - 36 / -(20)) + (-40 + (((-(-25 - -3)))) * 55)",
        "31/65.8+(-40++22*55)"
    ]
    for test in tests:
        print(my_eval(test), eval(test))
