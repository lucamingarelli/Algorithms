import numpy as np, matplotlib.pyplot as plt, seaborn as sns
import scipy.stats as st
sns.set()
plt.rcParams['text.latex.preamble'] = r'\usepackage{amsmath}' #for \text command
#Options
params = {'text.usetex' : True,
          # 'font.size' : 15
          }
plt.rcParams.update(params)

n = 1000
x1 = x2 = np.linspace(-2.7, 2.7, n)
dx = x1[-1] - x2[-2]
u1, u2 = st.norm.cdf(x1), st.norm.cdf(x2)

# COMONOTONICITY COPULA
def Co_cop(u1, u2):
    U1, U2 = np.meshgrid(u1, u2)
    return np.minimum(U1, U2)
# COUNTERMONOTONICITY COPULA
def Counter_cop(u1, u2):
    U1, U2 = np.meshgrid(u1, u2)
    return np.maximum(U1+U2-1,0)
# INDEPENDENCE COPULA
def Ind_cop(u1, u2):
    U1, U2 = np.meshgrid(u1, u2)
    return U1 * U2
# GAUSSIAN COPULA
def Gauss_cop(u1, u2, rho=0.5):
    """Rho in [0, 1]"""
    iU1, iU2 = np.meshgrid(st.norm.ppf(u1), st.norm.ppf(u2))
    jnt = st.multivariate_normal.cdf(np.array([iU1, iU2]).T,
                                     mean=[0, 0], cov=[[1, rho], [rho, 1]])
    return jnt
# T COPULA
def t_cop(u1, u2, rho=0.5, nu=3):
    """Nu > 0"""
    iU1, iU2 = np.meshgrid(st.t.ppf(u1, df=nu), st.t.ppf(u2, df=nu))
    mvt = st.multivariate_t(df=nu, loc=[0,0],
                            shape=np.array([[1, rho], [rho, 1]]) * nu/(nu-2))
    dens = mvt.pdf(x=np.array([iU1, iU2]).T)
    CDF = np.cumsum(np.cumsum(dens, axis=0), axis=1) * dx**2
    return CDF
# CLAYTON COPULA
def Clayton_cop(u1, u2, theta=1.):
    """Theta in [-1, inf[ \ {0}"""
    U1, U2 = np.meshgrid(u1, u2)
    jnt = (U1**(-theta) + U2**(-theta) - 1).clip(min=0) ** (-1/theta)
    return jnt
# GUMBEL COPULA
def Gumbel_cop(u1, u2, theta=1.):
    """Theta in [1, inf["""
    U1, U2 = np.meshgrid(u1, u2)
    jnt = np.exp(-((-np.log(U1))**theta
                   +(-np.log(U2))**theta)**(1/theta))
    return jnt
# JOE COPULA
def Joe_cop(u1, u2, theta=1.):
    """Theta in [1, inf["""
    U1, U2 = np.meshgrid(u1, u2)
    jnt = 1 - ((1-U1)**theta + (1-U2)**theta - ((1-U1)*(1-U2))**theta) ** (1/theta)
    return jnt
# FRANK COPULA
def Frank_cop(u1, u2, theta=1.):
    """Theta in [-inf, inf[ \ {0}"""
    U1, U2 = np.meshgrid(u1, u2)
    jnt = -(1/theta) * np.log(1+((np.exp(-theta*U1) - 1)*(np.exp(-theta*U2) - 1))/(np.exp(-theta) - 1))
    return jnt


# J = Gauss_cop(u1, u2, rho=0.9)
# J = t_cop(u1, u2, rho=0.9, nu=2)
J = Frank_cop(u1, u2, theta=5)
# J = Clayton_cop(u1, u2, theta=5)
dJ = (np.diff(np.diff(J).T).T).clip(min=1e-10)

plt.contour(u1, u2, J, 100); plt.show()
# plt.contour(x1, x2, J, 10); plt.show()
plt.contour(u1[1:], u2[1:], dJ, 10); plt.show()
plt.contour(x1[1:], x2[1:], dJ, 10); plt.show()

# WEBSITE PLOTS
# Co, Ind, Counter

fig, axes = plt.subplots(ncols=3, nrows=1, sharey=True, figsize=(4.5,2))
for C, ax, lb in zip([Counter_cop, Ind_cop, Co_cop], axes,
                     [r'$W_{\text{counter}}$', r'$C_{\text{ind}}$', r'$C_{\text{co}}$']):
    J = C(u1, u2)
    cs = ax.contour(u1, u2, J, 15, cmap='viridis')
    ax.set_xticks([0,1])
    ax.set_yticks([0, 1])
    ax.set_xlim(0, 1)
    ax.set_ylim(0, 1)
    ax.patch.set_alpha(0)
    ax.set_xlabel(lb)
plt.tight_layout()
plt.savefig('/Users/Luca/Downloads/co_ind_counter.svg', transparent=True)
plt.show()


# Grid of Copulae
JS, DJS = [], []
for cp, p in ([Gauss_cop, 0.5], [t_cop, 0.5],
              [Clayton_cop, 2], [Gumbel_cop, 2],
              [Joe_cop, 2], [Frank_cop, 5]):
    J = cp(u1, u2, p)
    dJ = (np.diff(np.diff(J).T).T).clip(min=1e-10) / (dx**2)
    DJS.append(dJ)

fig, axes = plt.subplots(ncols=3, nrows=2, sharex=True, sharey=True)
for dJ, ax, lb in zip(DJS, axes.flatten(),
                      (r'Gaussian, $\rho=0.5$', r'Student-t, $\rho=0.5$, $\nu=3$',
                       r'Clayton, $\theta=2$', r'Gumbel, $\theta=2$',
                       r'Joe, $\theta=2$', r'Frank, $\theta=5$')):
    v = np.linspace(0, dJ.max(), 10)
    cs = ax.contour(x1[1:], x2[1:], dJ, v, cmap='viridis', vmin=0, vmax=dJ.max())
    # plt.colorbar(cs, ax=ax)
    ax.patch.set_alpha(0)
    for s in ['right', 'left', 'top', 'bottom']:
        ax.spines[s].set_visible(False)
    ax.set_xlim(-2.7, 2.7)
    ax.set_ylim(-2.7, 2.7)
    ax.set_title(lb)
plt.tight_layout()
plt.savefig('/Users/Luca/Downloads/copulae_grid.svg', transparent=True)
plt.show()

# Tail Dependence
cmap = plt.cm.get_cmap('viridis', 6)
plt.figure(figsize=(6.5,4.5))
for n, ν in enumerate([1, 2, 5, 15, 50, 500]):
    ρ_vec = np.linspace(-.9999, .9999, 1000)
    Λ = 2 * st.t.cdf(-np.sqrt((ν + 1) * (1 - ρ_vec) / (1 + ρ_vec)), df=ν + 1)
    plt.plot(ρ_vec, Λ, label=r'$\nu = {}$'.format(ν), color=cmap(n), zorder=10-n)
plt.legend(ncol=3)
for s in ['right', 'left', 'top', 'bottom']:plt.gca().spines[s].set_visible(False)
plt.xlim(-1.001, 1.001)
plt.ylim(-0.001, 1.001)
plt.gca().patch.set_alpha(0)
plt.ylabel(r'$\lambda_u=\lambda_\ell$ for $C_{\nu, \rho}^t$')
plt.xlabel(r'$\rho$')
plt.tight_layout()
plt.savefig('/Users/Luca/Downloads/tail_dependence.svg', transparent=True)
plt.show()


# Attainable correlations
σ = np.linspace(1e-7,5,100)
ρ_min = (np.exp(-σ)-1) / np.sqrt((np.exp(1) - 1)*(np.exp(σ**2)-1))
ρ_max = (np.exp(σ)-1) / np.sqrt((np.exp(1) - 1)*(np.exp(σ**2)-1))

plt.figure(figsize=(6,3))
plt.plot(σ, ρ_min, color='tab:blue', label=r'$\rho_{\text{min}}$')
plt.plot(σ, ρ_max, color='tab:red', label=r'$\rho_{\text{max}}$')
plt.yticks([-1,-.5,0,.5,1])
plt.legend()
plt.xlabel(r'$\sigma$')
plt.ylabel(r'$\rho$')
plt.xlim(0, 5)
plt.ylim(-1, 1.005)
plt.gca().patch.set_alpha(0)
for s in ['right', 'left', 'top', 'bottom']:plt.gca().spines[s].set_visible(False)
plt.tight_layout()
plt.savefig('/Users/Luca/Downloads/attainable_corr.svg', transparent=True)
plt.show()


# NEW PLOTS

u1 = st.beta.cdf(x1, a=2, b=3)
u2 = st.beta.cdf(x2, a=2, b=3)

JS, DJS = [], []
for cp, p in ([Gauss_cop, 0.5], [t_cop, 0.5],
              [Clayton_cop, 2], [Gumbel_cop, 2],
              [Joe_cop, 2], [Frank_cop, 2]):
    J = cp(u1, u2, p)
    dJ = (np.diff(np.diff(J).T).T).clip(min=1e-10) / (dx**2)
    DJS.append(dJ)
fig, axes = plt.subplots(ncols=2, nrows=3, sharex=True, sharey=True)
for dJ, ax, lb in zip(DJS, axes.flatten(),
                      (r'Gaussian, $\rho=0.5$', r'Student-t, $\rho=0.5$, $\nu=3$',
                       r'Clayton, $\theta=2$', r'Gumbel, $\theta=2$',
                       r'Joe, $\theta=2$', r'Frank, $\theta=2$')):
    cs = ax.contour(x1[1:], x2[1:], dJ, cmap='viridis')
    # cs = ax.contour(u1, u2, J, 10, cmap='viridis')
    # plt.colorbar(cs, ax=ax)
    ax.patch.set_alpha(0)
    for s in ['right', 'left', 'top', 'bottom']:
        ax.spines[s].set_visible(False)
    ax.set_xlim(0, 1)
    ax.set_ylim(0, 1)
    ax.set_title(lb)
plt.savefig('/Users/Luca/Downloads/temp.svg', transparent=True)
plt.show()
