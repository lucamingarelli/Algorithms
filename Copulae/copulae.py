import numpy as np, pandas as pd, matplotlib.pyplot as plt, seaborn as sns
import matplotlib as ml
import scipy.stats as st
sns.set()
plt.rcParams['text.latex.preamble'] = r'\usepackage{amsmath}' #for \text command
#Options
params = {'text.usetex' : True}
plt.rcParams.update(params)
def website_style():
    for ax in [h.ax_joint, h.ax_marg_x, h.ax_marg_y]:
        ax.patch.set_alpha(0)
        for s in ['right', 'left', 'top', 'bottom']:
            ax.spines[s].set_visible(False)

    h.ax_marg_x.spines['bottom'].set_visible(True)
    h.ax_marg_y.spines['left'].set_visible(True)
    h.ax_marg_x.yaxis.set_major_formatter(plt.NullFormatter())
    h.ax_marg_y.xaxis.set_major_formatter(plt.NullFormatter())
    plt.tight_layout()
jnt_kw = dict(height=5, ratio=5, space=0,
              color="tab:blue", alpha=0)
N = 10_000_000

cmap = plt.cm.Blues
my_cmap = cmap(range(cmap.N))
my_cmap[:,-1] = np.linspace(0,10,cmap.N).clip(max=1)
my_cmap = ml.colors.ListedColormap(my_cmap) # Create new colormap


mvnorm = st.multivariate_normal(mean=[0, 0], cov=[[1., 0.75],
                                                  [0.75, 1.]])
# Generate random samples from multivariate normal with correlation .5
X = pd.DataFrame(mvnorm.rvs(N), columns=['X1', 'X2'])

h = (sns.jointplot(data=X, x='X1', y='X2', marginal_kws=dict(bins=30),
                   xlim=(-3,3), ylim=(-3,3), kind='hex', gridsize=2, **jnt_kw)
        .plot_joint(sns.kdeplot, shade=False, cmap='viridis'))
h.ax_joint.set_xlabel(r'$X_1$')
h.ax_joint.set_ylabel(r'$X_2$', rotation=0)
website_style()
plt.savefig('/Users/Luca/Downloads/joint_G.svg', transparent=True)
plt.show()


U = pd.DataFrame(st.norm().cdf(X), columns=['U1', 'U2'])

h = (sns.jointplot(data=U, x='U1', y='U2', marginal_kws=dict(bins=30),
                   xlim=(0,1), ylim=(0,1),kind='hex', gridsize=2, **jnt_kw)
        .plot_joint(plt.hexbin, gridsize=30, cmap=my_cmap))
h.ax_joint.set_xlabel(r'$U_1$')
h.ax_joint.set_ylabel(r'$U_2$', rotation=0)
website_style()
plt.savefig('/Users/Luca/Downloads/hex_copula.svg', transparent=True)
plt.show()

# h = sns.jointplot(data=U, x='U1', y='U2', marginal_kws=dict(bins=30),
#                   xlim=(0,1), ylim=(0,1), marker='.', alpha=0.25,
#                   height=5, ratio=5, space=0, color="tab:blue")
# h.ax_joint.set_xlabel(r'$U_1$')
# h.ax_joint.set_ylabel(r'$U_2$')
# website_style()
# plt.savefig('/Users/Luca/Downloads/hex_copula_scatter.svg', transparent=True)
# plt.show()


"""This joint plot above is usually how copulas are visualized.
Now we just transform the marginals again to what we want (Gumbel and Beta):"""

m1 = st.beta(a=3, b=10) # 10, 2
m2 = st.gumbel_l()

x1_t = m1.ppf(U.U1)
x2_t = m2.ppf(U.U2)
X_trans = pd.DataFrame([x1_t, x2_t], index=['XT1', 'XT2']).T

h = (sns.jointplot(data=X_trans, x='XT1', y='XT2', kind='hex', gridsize=2,
                   marginal_kws=dict(bins=30),
                   xlim=(0, 0.6), ylim=(-5, 2.0),
                   ** jnt_kw)
        .plot_joint(sns.kdeplot, cmap='viridis'))
h.ax_joint.set_xlabel(r"$\overset{\sim}{X}_1$")
h.ax_joint.set_ylabel(r"$\overset{\sim}{X}_2$", rotation=0)
website_style()
plt.savefig('/Users/Luca/Downloads/Gcop_gumbBeta_marginals.svg', transparent=True)
plt.show()

"""Contrast that with the joint distribution without correlations:
"""
x1, x2 = m1.rvs(N), m2.rvs(N)

h = (sns.jointplot(x=x1, y=x2, kind='hex', gridsize=2,
                   marginal_kws=dict(bins=30),
                   xlim=(0,0.6), ylim=(-5,2.0),
                   ** jnt_kw)
        .plot_joint(sns.kdeplot, cmap='viridis'))
h.ax_joint.set_xlabel(r"$\overset{\sim}{X}_1$")
h.ax_joint.set_ylabel(r"$\overset{\sim}{X}_2$", rotation=0)
website_style()
plt.savefig('/Users/Luca/Downloads/IndCop_gumbBeta_marginals.svg', transparent=True)
plt.show()

"""Finally use Clayton copula"""

N=10_000_000
θ = 2
def clayton(θ, N, d=2):
    Γ = st.gamma.rvs(a=1/θ, scale=1, size=N)
    X = st.uniform.rvs(size=(d,N))
    return (1 - np.log(X) / Γ) ** (-1/θ)


U1, U2 = clayton(θ=θ, N=N, d=2)

U = pd.DataFrame([U1, U2], index=['U1', 'U2']).T

h = (sns.jointplot(data=U, x='U1', y='U2', marginal_kws=dict(bins=30),
                   xlim=(0,1), ylim=(0,1),
                   kind='hex', gridsize=2, **jnt_kw)
        .plot_joint(plt.hexbin, gridsize=30, cmap=my_cmap))
h.ax_joint.set_xlabel(r'$U_1$')
h.ax_joint.set_ylabel(r'$U_2$', rotation=0)
website_style()
plt.savefig('/Users/Luca/Downloads/hex_Claytoncopula.svg', transparent=True)
plt.show()


x1_t = m1.ppf(U.U1)
x2_t = m2.ppf(U.U2)
X_trans = pd.DataFrame([x1_t, x2_t], index=['XT1', 'XT2']).T

h = (sns.jointplot(data=X_trans, x='XT1', y='XT2', kind='hex', gridsize=2,
                   marginal_kws=dict(bins=30),
                   xlim=(0, 0.6), ylim=(-6, 2.0),
                   ** jnt_kw)
        .plot_joint(sns.kdeplot, cmap='viridis'))
h.ax_joint.set_xlabel(r"$\overset{\sim}{X}_1$")
h.ax_joint.set_ylabel(r"$\overset{\sim}{X}_2$", rotation=0)
website_style()
plt.savefig('/Users/Luca/Downloads/Claytoncop_gumbBeta_marginals.svg', transparent=True)
plt.show()


"""And now using OpenTURNS"""

import openturns as ot

C = ot.ClaytonCopula(2)
X = ot.ComposedDistribution([ot.Uniform(0, 1),
                             ot.Uniform(0, 1)],
                            C)

U1, U2 = np.array(X.getSample(N)).T


sns.jointplot(U1, U2, kind='kde'); plt.show()


"""FITTING PARAMETRIC COPULAE: OpenTURNS"""
"""Multivariate Normal"""
import openturns.viewer as viewer
np.random.seed(seed=0)

N = 10_000
mvnorm = st.multivariate_normal(mean=[0, 0], cov=[[1., 0.75],
                                                  [0.75, 1.]])
X = mvnorm.rvs(N)

dist = ot.NormalCopulaFactory().build(X)
print(dist)
print(dist.getParameter())







"""Gaussian Copula, non-gaussian marginals"""
N = 10_000
mvnorm = st.multivariate_normal(mean=[0, 0], cov=[[1., 0.75],
                                                  [0.75, 1.]])
X = mvnorm.rvs(N)
U = st.norm().cdf(X)

X_trans = np.array([st.beta(a=3, b=10).ppf(U[:,0]),
                    st.gumbel_l().ppf(U[:,1])]).T

dist = ot.NormalCopulaFactory().build(X_trans)
print(dist)
print(dist.getParameter())

"""Non-gaussian Copula, non-gaussian marginals"""

C = ot.ClaytonCopula(2)
J = ot.ComposedDistribution([ot.Uniform(0, 1),
                             ot.Uniform(0, 1)],
                            C)
U1, U2 = np.array(J.getSample(10_000)).T

X_trans = np.array([st.beta(a=3, b=10).ppf(U1),
                    st.gumbel_l().ppf(U2)]).T
dist = ot.ClaytonCopulaFactory().build(X_trans)
print(dist)
print(dist.getParameter())


"""Estimation Monte Carlo"""
from tqdm import tqdm
mvnorm = st.multivariate_normal(mean=[0, 0], cov=[[1., 0.75],
                                                  [0.75, 1.]])
MN_estimates = []
G_BG_estimates = []
N_vec = np.logspace(3, 5, 1000)
for N in tqdm(N_vec):
    MN_estimates.append([])
    G_BG_estimates.append([])
    for mc in range(200):
        X = mvnorm.rvs(int(N))
        MN_estimates[-1].append(ot.NormalCopulaFactory()
                                  .build(X)
                                  .getParameter()[0])
        # U = st.norm().cdf(X)
        # X_trans = np.array([st.beta(a=3, b=10).ppf(U[:, 0]),
        #                     st.gumbel_l().ppf(U[:, 1])]).T
        # G_BG_estimates[-1].append(ot.NormalCopulaFactory()
        #                             .build(X_trans)
        #                             .getParameter()[0])

MN_estimates = np.array(MN_estimates)
# G_BG_estimates = np.array(G_BG_estimates)
######## Plot with errors
X = MN_estimates.mean(axis=1)
Xe = MN_estimates.std(axis=1)
# Y = G_BG_estimates.mean(axis=1)
# Ye = G_BG_estimates.std(axis=1)

plt.figure(figsize=(5, 3))
plt.plot(N_vec, X, color='tab:blue')
plt.fill_between(N_vec, X-Xe, X+Xe, color='tab:blue', alpha=0.3)
# plt.plot(N_vec, Y)
# plt.plot(N_vec, Y-Ye, Y+Ye)
plt.xlim(1e3, 1e5)
plt.xscale('log')
ax = plt.gca()
ax.patch.set_alpha(0)
for s in ['right', 'left', 'top', 'bottom']:
    ax.spines[s].set_visible(False)
plt.xlabel(r'$N$')
plt.ylabel(r'$\rho$', rotation=0)
plt.tight_layout()
plt.savefig('/Users/Luca/Downloads/Gcop_estimate_convergence.svg', transparent=True)
plt.show()


"""Non Parametric plots"""
N = 1_000
X = mvnorm.rvs(N)

dist = ot.NormalCopulaFactory().build(mvnorm.rvs(N*100))
dist_KS = ot.KernelSmoothing().build(X).getCopula()
dist_Bernstein = ot.EmpiricalBernsteinCopula(X, 20, False)

# Draw fitted distribution
def draw_dist(D):
    graph = D.drawPDF()
    cmap = plt.cm.get_cmap('Blues', len(graph.getColors()))
    colors = cmap(range(len(graph.getColors())))
    colors = ['#{:02x}{:02x}{:02x}'.format(int(r*255),
                                           int(g*255),
                                           int(b*255))
                    for r, g, b, a in colors]
    colors = colors[1:] + [colors[0]]
    graph.setColors(colors)
    return graph

# draw_dist(dist)
# view = viewer.View(dist_KS.drawPDF()); plt.show()
# draw_dist(dist_KS)
# draw_dist(dist_Bernstein)

# Three-plots
fig, [ax1, ax2, ax3] = plt.subplots(1, 3, figsize=(7, 8/3),
                                    sharey=True)
for D, ax, lb in zip([dist, dist_Bernstein, dist_KS],
                      [ax1, ax2, ax3],
                      ['Gaussian', 'Bernstein', 'Kernel Smoothing']):
    graph = draw_dist(D)
    view = viewer.View(graph, axes=[ax])
    ax.set_xlim(0, 1)
    ax.set_ylim(0, 1)
    ax.set_xlabel(r'$u_1$')
    ax.set_ylabel(None)
    ax.get_legend().remove()
    ax.title.set_text(lb)
    ax.patch.set_alpha(0)
    for s in ['right', 'left', 'top', 'bottom']:
        ax.spines[s].set_visible(False)
ax1.set_ylabel(r'$u_2$', rotation=0, labelpad=10)
fig.suptitle(None)
plt.tight_layout()
plt.savefig('/Users/Luca/Downloads/NonParametric_estimates.svg', transparent=True)
plt.show()


