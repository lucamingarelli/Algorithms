def next_gen(cells):
    n_rows, n_cols = len(cells), len(cells[0]) if cells else 0
    if n_cols == 0:  return cells

    def cell(cells, x, y):
        return cells[y][x] if 0 <= x < n_cols and 0 <= y < n_rows else 0

    def n_neighbors(cells, x, y):
        return sum(cell(cells, x + i, y + j) for (i, j) in
                   ((-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)))

    def cell_next(cells, x, y):
        return int(n_neighbors(cells, x, y) in ((2, 3) if cells[y][x] else (3,)))

    return [[cell_next(cells, x, y) for x in range(n_cols)] for y in range(n_rows)]



