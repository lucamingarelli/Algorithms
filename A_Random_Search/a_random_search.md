# A Random Search

$P$ people are tasked with finding *the-one-role* granting access to a website from a set of $N$ roles. Anyone can take up as many roles as desired.

They decide to use the following collaborative strategy: 
* each person is allocated $m$ random roles;
* those people who can now access the website know that *the-one-role* is among their $m$ roles;
* those who cannot access the website compute a union of their roles - call that *bad_set*;
* those who can access the website subtract *bad_set* from their sets of roles, then they compute the intersection of their resulting sets:
    - if the resulting set is of size 1, then its content is *the-one-role*

**What is the probability that *the-one-role* is found on the first attempt?**

