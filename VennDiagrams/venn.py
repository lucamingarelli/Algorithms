# Import the library
import matplotlib.pyplot as plt
from matplotlib_venn import venn3

# Make the diagram
venn_2013 = (38.9, 16.7, 31.8, 2.5, 7.7, 1.5, 6.2)
venn_2018 = (38.5, 19.0, 29.7, 2.8, 4.9, 0.6, 4.6)
venn_2019 = (33.6, 16, 32.6, 3.1, 4.9, 1.1, 3.4)
venn3(subsets=venn_2019,
      set_labels=('AMG18', 'BH20', 'ST17'))
# plt.title('2018')
plt.tight_layout()
plt.savefig('VennDiagrams/img/2019_venn.pdf')
plt.show()