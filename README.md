# Algorithms
#### A collection of nice algorithms and related problems.

## Useful resources
* [Structure and Interpretation of Computer Programs](https://mitpress.mit.edu/sites/default/files/sicp/full-text/book/book.html)
* [Exploring Fibonacci Formula in Python](https://levelup.gitconnected.com/an-exploration-of-fibonacci-in-python-ac4e7df40d55)