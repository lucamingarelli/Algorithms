import numpy as np
import scipy as sp
import matplotlib.pyplot as plt, seaborn as sns
sns.set()

def Eratosthenes_Sieve(n):
    """Eratosthenes Sieve
    Args:
         n (int)
    Returns: List of primes up to n
    """
    a = [False]*2 + [True] * (n-2)  # Initialise primality list
    for i, isprime in enumerate(a):
        if isprime:
            yield i
            for k in range(i * i, n, i):  # Mark factors non-prime
                a[k] = False
def primes(n):
    return [*Eratosthenes_Sieve(n)]

# Prime counting function
import sympy
π = lambda n: [sympy.ntheory.generate.primepi(k) for k in range(n)]

N = 1000
Ei = lambda x: sp.special.expi(x)
li = lambda x: Ei(np.log(x))
Li = lambda x: li(x) - li(2)

plt.plot(π(N))
x0 = np.arange(2, N)
plt.plot(x0, Li(x0), '--')
plt.show()

# Riemann Hypothesis

