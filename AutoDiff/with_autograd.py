import autograd.numpy as np  # Thinly-wrapped numpy
from autograd import grad    # The only autograd function you may ever need
from autograd import elementwise_grad as grad

def f(x):
    return 4*x**2 / (1-x)**3

def f(x):
    return np.sin(x)


grad_f = grad(grad(f));grad(f)
grad_f(3.0)


(grad(f))(3.0)
(grad(grad(f)))(3.0)
(grad(grad(grad(f))))(3.0)
(grad(grad(grad(grad(f)))))(3.0)


import matplotlib.pyplot as plt
x = np.linspace(-6,6,100)
plt.plot(x, np.cos(x))
plt.plot(x, (grad(f))(x), '--')
plt.plot(x, (grad(f))(x) - np.cos(x))
plt.show()