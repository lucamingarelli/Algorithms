import numpy as np
import scipy.linalg, scipy.sparse as sp
from functools import lru_cache

SUPERSCRIPTS = ["⁰", "¹", "²", "³", "⁴", "⁵", "⁶", "⁷", "⁸", "⁹"]

@lru_cache()
def _generate_epsilon(n=None, diff_order=1):
    row = np.arange(n - diff_order)
    col = row + diff_order
    data = np.ones(n - diff_order)
    ε = sp.coo_matrix((data, (row, col)), shape=(n, n)).toarray()
    return ε


class Dual:
    def __init__(self, *args):
        if not args:
            args = [0]
        self._params = args
        self.n = len(args)
        self.eps_all = [_generate_epsilon(self.n, k) for k in range(1, self.n)]

    def matrix_repr(self):
        return sum(p*ε for p, ε in zip(self._params[1:], self.eps_all)) + self._params[0]*np.eye(self.n)

    def from_matrix(self, M):
        self._params = M[0]
        self.n = len(self._params)
        self.eps_all = [_generate_epsilon(self.n, k) for k in range(1, self.n)]
        return self

    def __repr__(self):
        repr = f'{self._params[0]}'
        for k, p in enumerate(self._params[1:]):
            repr += f" + {p}ε{''.join([SUPERSCRIPTS[int(q)] for q in list(str(k + 1))]) if k > 0 else ''}"
        return repr

    def __add__(self, other):
        if isinstance(other, Dual):
            return Dual(*[p1 + p2 for p1, p2 in zip(self._params, other._params)])
        else:
            return Dual(*([self._params[0] + other] + list(self._params[1:])))

    def __radd__(self, other):
        return self + other

    def __neg__(self):
        return Dual(*[-p for p in self._params])

    def __sub__(self, other):
        return self.__add__(other.__neg__())

    def __rsub__(self, other):
        return -self + other

    def _mul_epsilon(self, k=1):
        if k==0: return self
        return Dual(*([0]*k + [p for p in self._params[:-k]]))

    def __mul__(self, other):
        if isinstance(other, Dual):
            return sum(self._mul_epsilon(k)*p2 for k, p2 in enumerate(other._params))
        else:
            return Dual(*[p * other for p in self._params])

    def __rmul__(self, other):
        return self * other

    def __pow__(self, power):
        if power<0:
            return self.__pow__(-power).inverse()
        elif power==0:
            return Dual(*([1]+[0]*(self.n-1)))
        elif power%2==0:
            return (self*self).__pow__(power/2)
        else:
            return self * (self).__pow__(power-1)

    def inverse(self):
        a = self._params[0]
        D = Dual(*([0]+list(self._params[1:])))
        return sum((-D / a)**k for k in range(self.n)) / a

    def __truediv__(self, other):
        if isinstance(other, Dual):
            return self * other.inverse()
        else:
            return Dual(*[p / other for p in self._params])

    def __rtruediv__(self, other):
        return self.inverse() * other

    def exp(self):
        # return sum(self**k/np.math.factorial(k) for k in range(self.n))
        return Dual(*([np.exp(self._params[0])] +
                      [np.exp(self._params[0]) * p / np.math.factorial(n + 1)
                       for n, p in enumerate(self._params[1:])]))

    def sin(self):
        # return sum(self**(2*k+1) * (-1) ** k / np.math.factorial(2*k+1)
        #            for k in range(self.n+1))
        f_list = [np.cos, lambda k: -np.sin(k), lambda k: -np.cos(k), np.sin]
        return Dual(*([np.sin(self._params[0])] +
                      [f(self._params[0])*p / np.math.factorial(n+1)
                       for n, (p,f) in enumerate(zip(self._params[1:],
                                            f_list*self.n))]))

    def cos(self):
        return (np.pi/2 - self).sin()




D = Dual(3,1,0,0,0,0,0,0,0)
f_Dual = 4*D**2 / (1-D)**3
f_Dual.matrix_repr() == (4*D.matrix_repr()@D.matrix_repr()) @ np.linalg.inv(((1-D)**3).matrix_repr())
f_Dual.matrix_repr()[0]


import matplotlib.pyplot as plt, seaborn as sns
sns.set()



# WEBSITE EXAMPLE 1
def f(x):
    return x**5

x = np.linspace(0,2,100)
Dual_f = f(Dual(x, 1, 0, 0, 0))
Df = [f(x), 5*x**4, 20*x**3, 60*x**2, 120*x]

plt.plot(x, Df[0])
plt.plot(x, Dual_f._params[0], '--')
plt.show()

k = 4
plt.plot(x, Df[k])
plt.plot(x, Dual_f._params[k]*np.math.factorial(k), '--')
plt.show()



# EXAMPLE 2
def f(x):
    return (x**2) / (x**2 - x +1)


x = np.linspace(0, 2, 100)
Dual_f = f(Dual(x, 1, 0, 0, 0))
Df = [f(x),
      -x*(x-2) / (x**2 - x +1)**2,
      2*(x**3-3*x**2+1)/(x**2 - x +1)**3,
      -6*(x**4-4*x**3+4*x-1)/(x**2 - x +1)**4,
      24*x*(x**4-5*x**3+10*x-5)/(x**2 - x +1)**5]

k =3
plt.plot(x, Df[k])
plt.plot(x, Dual_f._params[k]*np.math.factorial(k), '--')
plt.show()










# EXAMPLE 3
def f(x):
    if isinstance(x, Dual): return (x).sin()**2/ (x**2 - x +1)
    else: return np.sin(x)**2/ (x**2 - x +1)

x = np.linspace(0, 2, 100)
Dual_f = f(Dual(x, 1, 1, 1, 1))
Df = [f(x),
(np.sin(x) *(2 *(x**2 - x + 1)* np.cos(x) + (1 - 2* x)* np.sin(x)))/(x**2 - x + 1)**2
      ]

k = 1
plt.plot(x, Df[k])
plt.plot(x, Dual_f._params[k]*np.math.factorial(k), '--')
# plt.plot(x, np.diff(f(x), append=f(2+(x[1]-x[0])))/(x[1]-x[0]))
plt.show()

df = (np.sin(x) *(2 *(x**2 - x + 1)* np.cos(x) + (1 - 2* x)* np.sin(x)))/(x**2 - x + 1)**2
print('AD error: ', np.abs(df - Dual_f._params[1]).max())
print('FD error: ', np.abs(df - np.gradient(f(x),x)).max())


plt.rcParams['text.latex.preamble'] = r'\usepackage{amsmath}' #for \text command
#Options
params = {'text.usetex' : True}
plt.rcParams.update(params)

plt.figure(figsize=(5,3.5))
for k in [0,1,2,3]:
    plt.plot(x, Dual_f._params[k]*np.math.factorial(k), label=0)
lgd = plt.legend([r'$f(x)$', r"$f'(x)$", r"$f''(x)$", r"$f'''(x)$"],
                 loc='lower right')
plt.xlabel(r'$x$')
plt.xlim(0,2)
for s in ['right', 'left', 'top', 'bottom']:
    plt.gca().spines[s].set_visible(False)
plt.gca().patch.set_alpha(0)
plt.tight_layout()
plt.savefig('/Users/Luca/Downloads/autodiff.svg', transparent=True,
            bbox_extra_artists=(lgd,), bbox_inches='tight')
plt.show()

